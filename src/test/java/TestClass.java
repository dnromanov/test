import org.testng.Assert;
import org.testng.SkipException;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class TestClass {
    @Test
    public void testFailed(){
        Assert.assertTrue(false);
    }
    @Test
    public void testSuccess(){
        Assert.assertTrue(true);
    }
    @Test
    public void testSkip(){
        throw new SkipException("Skipping the test case");
    }
}
